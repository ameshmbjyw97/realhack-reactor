package com.reactor.realhack.backend.repository;

import com.reactor.realhack.backend.entity.UserQuestionVote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserQuestionVoteRepository extends CrudRepository<UserQuestionVote,Integer> {

    UserQuestionVote findByIdAndUserID(int id,int userID);
}
