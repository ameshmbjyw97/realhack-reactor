package com.reactor.realhack.backend.repository;

import com.reactor.realhack.backend.entity.Question;
import com.reactor.realhack.backend.service.util.enums.Question_Status;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question,Integer> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE question  set question_status =: question_status where id =: id",nativeQuery = true)
    boolean setQuestionStatus(@Param("question_status") String question_status,@Param("id") int id);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE question  set up_votes = up_votes + 1 where id =: questionID",nativeQuery = true)
    boolean addUpVote(@Param("questionID") int questionID);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE question set up_votes = up_votes - 1 where id =: questionID",nativeQuery = true)
    boolean minusUpVote(@Param("questionID") int questionID);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE question  set down_votes = down_votes + 1 where id =: questionID",nativeQuery = true)
    boolean addDownVote(@Param("questionID") int questionID);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE question  set down_votes = down_votes - 1 where id =: questionID",nativeQuery = true)
    boolean minusDownVote(@Param("questionID") int questionID);


}
