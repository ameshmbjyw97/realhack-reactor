package com.reactor.realhack.backend.repository;

import com.reactor.realhack.backend.entity.Answer;
import com.reactor.realhack.backend.entity.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends CrudRepository<Answer,Integer> {
    List<Answer> findByQuestion(Question question);
}
