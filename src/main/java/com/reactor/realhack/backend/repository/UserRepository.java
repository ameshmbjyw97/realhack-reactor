package com.reactor.realhack.backend.repository;

import com.reactor.realhack.backend.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,Integer> {
    User findByEmail(String email);

    User findById(int id);
}
