package com.reactor.realhack.backend.controller;

import com.reactor.realhack.backend.model.ImageDTO;
import com.reactor.realhack.backend.model.UserDTO;
import com.reactor.realhack.backend.security.AppConstant;
import com.reactor.realhack.backend.security.TokenUtils;
import com.reactor.realhack.backend.security.model.AuthenticationRequest;
import com.reactor.realhack.backend.security.model.AuthenticationResponse;
import com.reactor.realhack.backend.security.model.SpringSecurityUser;
import com.reactor.realhack.backend.service.UserService;
import com.reactor.realhack.backend.service.util.ImagePath;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/authentication")
public class AuthenticationController {


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    private PasswordEncoder passwordEncoder;

    @Autowired
    private ImagePath imagePath;

    @PostMapping(value = "/auth")
    public ResponseEntity<?> authenticationRequest(@RequestBody AuthenticationRequest authenticationRequest)
            throws AuthenticationException {

        // Perform the authentication
        Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-authentication so we can generate token
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String token = this.tokenUtils.generateToken(userDetails);

        String username = authenticationRequest.getUsername();

        AuthenticationResponse authenticationResponse = new AuthenticationResponse();

        authenticationResponse.setToken(token);
        authenticationResponse.setObject(userService.findUserByEmail(username));

        // Return the token
        return ResponseEntity.ok(authenticationResponse);
    }

    @PostMapping("/registerUser")
    public UserDTO registerUser(@RequestBody UserDTO userDTO){
        return userService.registerUser(userDTO);
    }


    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> authenticationRequest(HttpServletRequest request) {
        String token = request.getHeader(AppConstant.tokenHeader);
        String username = this.tokenUtils.getUsernameFromToken(token);
        SpringSecurityUser user = (SpringSecurityUser) this.userDetailsService.loadUserByUsername(username);
        if (this.tokenUtils.canTokenBeRefreshed(token, user.getLastPasswordReset())) {
            String refreshedToken = this.tokenUtils.refreshToken(token);
            return ResponseEntity.ok(new AuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }


    @GetMapping(value = "images/get/{fileName:.+}/", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] getImage(@PathVariable("fileName") String fileName) throws IOException {

        System.out.println("========= FILE NAME  " + fileName);

        try {
            return readBytesFromFile(imagePath.getProfileImagePath() + fileName.trim());

        } catch (Exception e) {
            System.out.println("=======" + e.getMessage());
        }

        return null;

    }

    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }

    @PostMapping("images/upload")
    public boolean fileUpload(@RequestBody ImageDTO imageDTO) {
        System.out.println("image file size: " + imageDTO.getFile().length());
        System.out.println(imageDTO.getFileName());


        byte[] bytes = new byte[0];
        Path path = null;

        if (imageDTO.getFile() != null) {
            String s = imageDTO.getFile();
            try {
                bytes = Base64.decode(s);
            } catch (Base64DecodingException e) {
                e.printStackTrace();
                System.out.println("ERROR : " + e.getMessage());
            }
        }
        System.out.println("image dto:" + imageDTO);
        String directory = imagePath.getProfileImagePath();
        System.out.println("Directory ======== " + directory);
        path = Paths.get(directory + imageDTO.getFileName());
        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;


    }

}
