package com.reactor.realhack.backend.controller;


import com.reactor.realhack.backend.model.AnswerDTO;
import com.reactor.realhack.backend.model.ClassificationDTO;
import com.reactor.realhack.backend.model.QuestionDTO;
import com.reactor.realhack.backend.model.Test;
import com.reactor.realhack.backend.security.TokenUtils;
import com.reactor.realhack.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping("/createQuestion")
    public QuestionDTO createQuestion(@RequestBody QuestionDTO questionDTO) {
        return userService.createQuestion(questionDTO);
    }

    @PostMapping("/provideAnswer")
    public AnswerDTO provideAnswer(@RequestBody AnswerDTO answerDTO) {
        return userService.provideAnswer(answerDTO);
    }

    @GetMapping("/markQuestionsAsAnswered")
    public boolean markQuestionsAsAnswered(@RequestParam("questionID") int questionID) {
        return userService.markQuestionAsAnswered(questionID);
    }

    @GetMapping("/addUpVote")
    public boolean addUpVote(@RequestParam("questionID") int questionID, @RequestParam("userID") int userID) {
        return userService.voteUpAdd(questionID, userID);
    }

    @GetMapping("/addDownVote")
    public boolean addDownVote(@RequestParam("questionID") int questionID, @RequestParam("userID") int userID) {
        return userService.voteDownAdd(questionID, userID);
    }

    @GetMapping("/minusUpVote")
    public boolean minusUpVote(@RequestParam("questionID") int questionID, @RequestParam("userID") int userID) {
        return userService.voteUpMinus(questionID, userID);
    }

    @GetMapping("/minusDownVote")
    public boolean minusDownVote(@RequestParam("questionID") int questionID, @RequestParam("userID") int userID) {
        return userService.voteDownMinus(questionID, userID);
    }

    @GetMapping("/getAllQuestionsWithAnswers")
    public List<QuestionDTO> getAllQuestionsWithAnswers() {
        return userService.getAllQuestionsWithAnswers();
    }

    @GetMapping("/getUserQuestionStatus")
    public Object getUserQuestionStatus(@RequestParam("questionID") int questionID, @RequestParam("userID") int userID) {
        return userService.getUserQuestionStatus(questionID, userID);
    }

    @PostMapping("/test")
    public Object test(@RequestBody Test test) {
        return post("https://southeastasia.api.cognitive.microsoft.com/contentmoderator/moderate/v1.0/ProcessText/Screen?classify=True",test.getValue());
    }

    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;

    public UserController() {
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "text/plain");
//        headers.add("Accept", "*/*");
        headers.add("Ocp-Apim-Subscription-Key", "123");



    }

    public Object post(String uri, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
        ResponseEntity<Object> responseEntity = rest.exchange(uri, HttpMethod.POST, requestEntity, Object.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }


}


