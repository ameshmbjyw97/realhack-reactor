package com.reactor.realhack.backend.entity;

import com.reactor.realhack.backend.service.util.enums.Question_Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "QUESTION")
public class Question extends SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "TITLE",nullable = false)
    private String title;
    @Column(name = "DESCRIPTION",nullable = false)
    private String description;
    @Column(name = "DATE",nullable = false)
    private String date;
    @Column(name = "QUESTION_STATUS",nullable = false)
    private String question_status;
    @Column(name = "UP_VOTES",nullable = false)
    private int up_votes;
    @Column(name = "DOWN_VOTES",nullable = false)
    private int down_votes;

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    @OneToMany(mappedBy = "question")
    private List<Answer> answers = new ArrayList<>();


    public Question() {
    }

    public Question(int id) {
        this.id =id;
    }

    public Question(String title, String description, String date, String question_status, int up_votes, int down_votes, User user) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.question_status = question_status;
        this.up_votes = up_votes;
        this.down_votes = down_votes;
        this.user = user;
    }

    public Question(String title, String description, String date, String question_status, int up_votes, int down_votes, User user, List<Answer> answers) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.question_status = question_status;
        this.up_votes = up_votes;
        this.down_votes = down_votes;
        this.user = user;
        this.answers = answers;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuestion_status() {
        return question_status;
    }

    public void setQuestion_status(String question_status) {
        this.question_status = question_status;
    }

    public int getUp_votes() {
        return up_votes;
    }

    public void setUp_votes(int up_votes) {
        this.up_votes = up_votes;
    }

    public int getDown_votes() {
        return down_votes;
    }

    public void setDown_votes(int down_votes) {
        this.down_votes = down_votes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }


}
