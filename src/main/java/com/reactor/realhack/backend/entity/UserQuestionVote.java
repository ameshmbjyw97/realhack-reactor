package com.reactor.realhack.backend.entity;

import com.reactor.realhack.backend.service.util.enums.Vote_Status;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "USER_QUESTION_VOTE")
public class UserQuestionVote extends SuperEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",nullable = false)
    private int id;
    @Column(name = "USER_ID",nullable = false)
    private int userID;
    @Column(name = "QUESTION_ID",nullable = false)
    private int questionID;
    @Column(name = "VOTE_STATUS",nullable = false)
    private String vote_status;

    public UserQuestionVote() {
    }

    public UserQuestionVote(int userID, int questionID, String vote_status) {
        this.userID = userID;
        this.questionID = questionID;
        this.vote_status = vote_status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getVote_status() {
        return vote_status;
    }

    public void setVote_status(String vote_status) {
        this.vote_status = vote_status;
    }
}
