package com.reactor.realhack.backend.model;


import org.springframework.web.multipart.MultipartFile;

public class ImageDTO {

    String file;
    String fileName;

    public ImageDTO() {
    }

    public ImageDTO(String file, String fileName) {
        this.file = file;
        this.fileName = fileName;
    }




    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
