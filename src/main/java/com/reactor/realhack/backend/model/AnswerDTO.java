package com.reactor.realhack.backend.model;

public class AnswerDTO extends SuperDTO {

    private int id;
    private String answer;
    private int userID;
    private int questionID;

    private UserDTO userDTO;

    public AnswerDTO() {
    }

    public AnswerDTO(int id, String answer, UserDTO userDTO) {
        this.id = id;
        this.answer = answer;
        this.userDTO = userDTO;
    }

    public AnswerDTO(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public AnswerDTO(int id, String answer, int userID) {
        this.id = id;
        this.answer = answer;
        this.userID = userID;
    }

    public AnswerDTO(int id, String answer, int userID, int questionID) {
        this.id = id;
        this.answer = answer;
        this.userID = userID;
        this.questionID = questionID;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
