package com.reactor.realhack.backend.model;

public class ClassificationDTO {
     private Review Classification;

    public ClassificationDTO() {
    }

    public ClassificationDTO(Review classification) {
        Classification = classification;
    }

    public Review getClassification() {
        return Classification;
    }

    public void setClassification(Review classification) {
        Classification = classification;
    }
}
