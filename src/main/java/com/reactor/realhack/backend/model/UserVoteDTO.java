package com.reactor.realhack.backend.model;

public class UserVoteDTO {
    private String status;

    public UserVoteDTO() {
    }

    public UserVoteDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
