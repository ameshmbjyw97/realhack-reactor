package com.reactor.realhack.backend.model;

import java.util.ArrayList;
import java.util.List;

public class UserDTO extends SuperDTO {

    private int id;
    private String name;
    private String email;
    private String password;
    private String profileImageUrl;

    private List<QuestionDTO> questions = new ArrayList<>();
    private List<AnswerDTO> answers = new ArrayList<>();

    public UserDTO() {
    }

    public UserDTO(int id, String name, String email, String password, String profileImageUrl) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.profileImageUrl = profileImageUrl;
    }

    public UserDTO(int id, String name, String email, String password, String profileImageUrl, List<QuestionDTO> questions, List<AnswerDTO> answers) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.profileImageUrl = profileImageUrl;
        this.questions = questions;
        this.answers = answers;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }
}
