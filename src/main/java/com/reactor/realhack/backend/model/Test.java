package com.reactor.realhack.backend.model;

public class Test {
    private String value;

    public Test() {
    }

    public Test(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
