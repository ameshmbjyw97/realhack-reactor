package com.reactor.realhack.backend.model;

import com.reactor.realhack.backend.service.util.enums.Question_Status;

import java.util.ArrayList;
import java.util.List;

public class QuestionDTO extends SuperDTO {


    private int id;
    private String title;
    private String description;
    private String date;
    private Question_Status question_status;
    private int up_votes;
    private int down_votes;
    private int  userID;
    private UserDTO userDTO;
    private String message;

    private List<AnswerDTO> answers = new ArrayList<>();

    public QuestionDTO() {
    }

    public QuestionDTO(int id, String title, String description, String date, Question_Status question_status, int up_votes, int down_votes, UserDTO userDTO) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.question_status = question_status;
        this.up_votes = up_votes;
        this.down_votes = down_votes;
        this.userDTO = userDTO;
    }

    public QuestionDTO(int id, String title, String description, String date, Question_Status question_status, int up_votes, int down_votes, UserDTO userDTO, List<AnswerDTO> answers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.question_status = question_status;
        this.up_votes = up_votes;
        this.down_votes = down_votes;
        this.userDTO = userDTO;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Question_Status getQuestion_status() {
        return question_status;
    }

    public void setQuestion_status(Question_Status question_status) {
        this.question_status = question_status;
    }

    public int getUp_votes() {
        return up_votes;
    }

    public void setUp_votes(int up_votes) {
        this.up_votes = up_votes;
    }

    public int getDown_votes() {
        return down_votes;
    }

    public void setDown_votes(int down_votes) {
        this.down_votes = down_votes;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
