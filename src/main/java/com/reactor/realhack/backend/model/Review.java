package com.reactor.realhack.backend.model;

public class Review {
    private boolean ReviewRecommended;

    public Review() {
    }

    public Review(boolean reviewRecommended) {
        ReviewRecommended = reviewRecommended;
    }

    public boolean isReviewRecommended() {
        return ReviewRecommended;
    }

    public void setReviewRecommended(boolean reviewRecommended) {
        ReviewRecommended = reviewRecommended;
    }
}
