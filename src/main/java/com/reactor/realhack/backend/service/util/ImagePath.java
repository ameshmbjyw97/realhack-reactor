package com.reactor.realhack.backend.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class ImagePath {


    @Autowired
    Environment environment;

    public ImagePath() {
    }

    public String getBaseFolderPath() {
        String image_saving_path = environment.getProperty("image_saving_path");
        return image_saving_path;
    }

    public String getProfileImagePath() {
        String image_saving_path = environment.getProperty("image_saving_path");
        String profile_images = environment.getProperty("profile_images");
        return image_saving_path + profile_images;
    }



    public Environment getEnvironment() {
        return environment;
    }
}
