package com.reactor.realhack.backend.service.impl;

import com.reactor.realhack.backend.controller.UserController;
import com.reactor.realhack.backend.entity.Answer;
import com.reactor.realhack.backend.entity.Question;
import com.reactor.realhack.backend.entity.User;
import com.reactor.realhack.backend.entity.UserQuestionVote;
import com.reactor.realhack.backend.model.*;
import com.reactor.realhack.backend.repository.AnswerRepository;
import com.reactor.realhack.backend.repository.QuestionRepository;
import com.reactor.realhack.backend.repository.UserQuestionVoteRepository;
import com.reactor.realhack.backend.repository.UserRepository;
import com.reactor.realhack.backend.service.UserService;
import com.reactor.realhack.backend.service.util.enums.Question_Status;
import com.reactor.realhack.backend.service.util.enums.Vote_Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserQuestionVoteRepository questionVoteRepository;

    private PasswordEncoder passwordEncoder;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");

    public UserServiceImpl(){
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    @Override
    public UserDTO registerUser(UserDTO userDTO) {

        User user = new User(userDTO.getId(),userDTO.getName(),userDTO.getEmail(),passwordEncoder.encode(userDTO.getPassword()),userDTO.getName().trim()+".jpeg");

        if(userRepository.save(user) != null){
            return new UserDTO(user.getId(),user.getName(),user.getEmail(),user.getPassword(),user.getProfileImageUrl());
        }else{
            return null;
        }
    }


    @Override
    public QuestionDTO createQuestion(QuestionDTO questionDTO) {
        //UserDTO userDTO = questionDTO.getUserDTO();

        Object object = userController.post("https://southeastasia.api.cognitive.microsoft.com/contentmoderator/moderate/v1.0/ProcessText/Screen?classify=True",questionDTO.getDescription());

        ClassificationDTO classificationDTO = (ClassificationDTO) object;

        if(!classificationDTO.getClassification().isReviewRecommended()){
            Question question = new Question(questionDTO.getTitle(),questionDTO.getDescription(),simpleDateFormat.format(new Date()), Question_Status.NOT_ANSWERD.toString(),0,0,userRepository.findById(questionDTO.getUserID()));
            if( questionRepository.save(question) != null){
                return questionDTO;
            }else{
                return null;
            }
        }else{
            QuestionDTO questionDTO1 = new QuestionDTO();
            questionDTO1.setMessage("OFFENCE");
            return questionDTO1;
        }

    }

    @Override
    public AnswerDTO provideAnswer(AnswerDTO answerDTO) {

        Answer answer = new Answer(answerDTO.getAnswer(),userRepository.findById(answerDTO.getUserID()),new Question(answerDTO.getQuestionID()));

        if(answerRepository.save(answer) != null){
            return answerDTO;
        }else{
            return null;
        }
    }


    @Override
    public boolean markQuestionAsAnswered(int questionID) {
        return questionRepository.setQuestionStatus(Question_Status.ANSWERED.toString(),questionID);
    }


    @Override
    public boolean voteUpAdd(int questionID, int userID) {

     UserQuestionVote questionVote =  questionVoteRepository.save(new UserQuestionVote(userID,questionID, Vote_Status.UP.toString()));
        if(questionVote != null){
            questionRepository.addUpVote(questionID);
                return true;
        }
        return false;
    }

    @Override
    public boolean voteDownAdd(int questionID, int userID) {
        if(questionVoteRepository.save(new UserQuestionVote(userID,questionID, Vote_Status.DOWN.toString())) != null){
            questionRepository.addDownVote(questionID);
                return true;
        }
        return false;
    }

    @Override
    public boolean voteUpMinus(int questionID, int userID) {
        if(questionVoteRepository.save(new UserQuestionVote(userID,questionID, Vote_Status.UP_NOT.toString())) != null){
            questionRepository.minusUpVote(questionID);
                return true;
        }
        return false;
    }

    @Override
    public boolean voteDownMinus(int questionID, int userID) {
        if(questionVoteRepository.save(new UserQuestionVote(userID,questionID, Vote_Status.DOOWN_NOT.toString())) != null){
            questionRepository.minusDownVote(questionID);
            return true;
        }
        return false;
    }

    @Override
    public List<QuestionDTO> getAllQuestionsWithAnswers() {

        List<QuestionDTO> questionDTOS = new ArrayList<>();

        for(Question question : questionRepository.findAll()){

            User user = userRepository.findById(question.getUser().getId());

            System.out.println("===================== ************** " + user.getName());

            Question_Status question_status = null;
            if(question.getQuestion_status().trim().equals("ANSWERED")){
                question_status = Question_Status.ANSWERED;
            }else if(question.getQuestion_status().trim().equals("NOT_ANSWERD")){
                question_status = Question_Status.NOT_ANSWERD;
            }


            QuestionDTO questionDTO = new QuestionDTO(question.getId(),question.getTitle(),question.getDescription(),question.getDate(),question_status,question.getUp_votes(),question.getDown_votes(),new UserDTO(user.getId(),user.getName(),user.getEmail(),user.getPassword(),user.getProfileImageUrl()));

            System.out.println("----------------- " +  questionDTO.getDescription() + questionDTO.getUserDTO().getName());

            List<Answer> answers = answerRepository.findByQuestion(question);

            List<AnswerDTO> answerDTOS = new ArrayList<>();

            for(Answer answer : answers){
                User user1 = answer.getUser();
                answerDTOS.add(new AnswerDTO(answer.getId(),answer.getAnswer(),new UserDTO(user1.getId(),user1.getName(),user1.getEmail(),user1.getPassword(),user1.getProfileImageUrl())));
            }

            questionDTO.setAnswers(answerDTOS);

            questionDTOS.add(questionDTO);
        }

        return questionDTOS;
    }

    @Override
    public UserDTO findByID(int id) {
        User user = userRepository.findById(id);
        return new UserDTO(user.getId(),user.getName(),user.getEmail(),user.getPassword(),user.getProfileImageUrl());
    }

    @Override
    public UserVoteDTO getUserQuestionStatus(int questionID, int userID) {
        if(questionVoteRepository.findByIdAndUserID(questionID,userID) == null){
            return new UserVoteDTO(Vote_Status.NOT.toString());
        }else{
           return new UserVoteDTO(questionVoteRepository.findByIdAndUserID(questionID,userID).getVote_status().toString());
        }
    }
}
