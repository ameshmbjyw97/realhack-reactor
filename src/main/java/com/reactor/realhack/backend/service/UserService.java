package com.reactor.realhack.backend.service;

import com.reactor.realhack.backend.entity.User;
import com.reactor.realhack.backend.model.AnswerDTO;
import com.reactor.realhack.backend.model.QuestionDTO;
import com.reactor.realhack.backend.model.UserDTO;
import com.reactor.realhack.backend.model.UserVoteDTO;

import java.util.List;

public interface UserService {

    User findUserByEmail(String email);
    UserDTO registerUser(UserDTO userDTO);

    QuestionDTO createQuestion(QuestionDTO questionDTO);
    AnswerDTO provideAnswer(AnswerDTO answerDTO);

    boolean markQuestionAsAnswered(int questionID);

    boolean voteUpAdd(int questionID,int userID);

    boolean voteUpMinus(int questionID,int userID);

    boolean voteDownAdd(int questionID,int userID);

    boolean voteDownMinus(int questionID,int userID);

    List<QuestionDTO> getAllQuestionsWithAnswers();

    UserDTO findByID(int id);

    UserVoteDTO getUserQuestionStatus(int questionID, int userID);

}
